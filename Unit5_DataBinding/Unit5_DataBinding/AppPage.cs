﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5_DataBinding
{
    public enum AppPage
    {
        Login,
        Info,
        Product,
        ProductDetail
    }
}
